﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SpaceshipGame
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D shipSprite;
        Texture2D asteroidSprite;
        Texture2D spaceBackground;

        SpriteFont spaceFont;
        SpriteFont timerFont;

        Ship ship = new Ship();
        Controller gameController = Controller.Instance;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;

            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            shipSprite = Content.Load<Texture2D>("ship");
            asteroidSprite = Content.Load<Texture2D>("asteroid");
            spaceBackground = Content.Load<Texture2D>("space");

            spaceFont = Content.Load<SpriteFont>("spaceFont");
            timerFont = Content.Load<SpriteFont>("timerFont");
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            ship.updateShip(gameTime);
            gameController.updateController(gameTime);

            for (int i = 0; i < gameController.asteroids.Count; i++)
            {
                gameController.asteroids[i].asteroidUpdate(gameTime);
                int sum = gameController.asteroids[i].radius + ship.radius;
                if (Vector2.Distance(gameController.asteroids[i].position, ship.position) < sum)
                {
                    gameController.inGame = false;
                    ship.position = Ship.defaultPosition;
                    gameController.asteroids.Clear();
                    break;
                }
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            spriteBatch.Draw(spaceBackground, new Vector2(0, 0), Color.White);
            spriteBatch.Draw(shipSprite, new Vector2(ship.position.X - 34, ship.position.Y - 50), Color.White);

            if (gameController.inGame == false)
            {
                string menuMessage = "Press enter to begin";
                Vector2 messageLength = spaceFont.MeasureString(menuMessage);
                spriteBatch.DrawString(spaceFont, menuMessage, new Vector2(640 - messageLength.X / 2, 200), Color.White);
            }

            for (int i = 0; i < gameController.asteroids.Count; i++)
            {
                spriteBatch.Draw(
                    asteroidSprite,
                    new Vector2(
                        gameController.asteroids[i].position.X - gameController.asteroids[i].radius,
                        gameController.asteroids[i].position.Y - gameController.asteroids[i].radius
                    ),
                    Color.White
                );
            }

            spriteBatch.DrawString(timerFont, $"Timer: {Math.Floor(gameController.totalTime)}", new Vector2(30, 40), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}