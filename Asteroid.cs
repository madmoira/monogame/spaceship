using System;

using Microsoft.Xna.Framework;

namespace SpaceshipGame
{
    public class Asteroid
    {
        public Vector2 position;
        public int speed = 220;
        public int radius = 59;
        static Random rand = new Random();

        public Asteroid(int newSpeed = 220)
        {
            speed = newSpeed;
            position = new Vector2(1280, rand.Next(0, 721));
        }

        public void asteroidUpdate(GameTime gameTime)
        {
            var dt = (float)gameTime.ElapsedGameTime.TotalSeconds;
            position.X -= speed * dt;
        }
    }
}