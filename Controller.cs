using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace SpaceshipGame
{
    public class Controller
    {
        protected static Controller instance = null;

        public List<Asteroid> asteroids = new List<Asteroid>();
        private double counter = 2D;
        private double maxTime = 2D;
        private int speed = 300;
        private int nextSpeed = 300;
        public float totalTime = 0f;

        public bool inGame = false;

        public static Controller Instance
        {
            get
            {
                if (instance == null)
                    instance = new Controller();
                return instance;
            }
        }

        public void updateController(GameTime gameTime)
        {
            if (inGame)
            {
                counter -= gameTime.ElapsedGameTime.TotalSeconds;
                totalTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            else
            {
                KeyboardState kState = Keyboard.GetState();
                if (kState.IsKeyDown(Keys.Enter))
                {
                    inGame = true;
                    totalTime = 0f;
                    counter = 2D;
                    maxTime = 2D;
                    speed = 300;
                    nextSpeed = 300;
                }
            }

            if (asteroids.Count > 0 && asteroids[0].position.X + asteroids[0].radius < 0)
                asteroids.RemoveAt(0);

            if (counter <= 0)
            {
                asteroids.Add(new Asteroid(speed));
                counter = maxTime;
                speed = nextSpeed;
                if (maxTime > 0.5)
                    maxTime -= 0.1D;

                if (nextSpeed < 720)
                    nextSpeed += 10;
            }
        }
    }
}